------------
-- create-draft.sql
--
-- create the tables for a nfl draft database
--
-- by: Shyaka Laniesse
-- last modified: 2016-03-24
------------

----------
-- nfl team table contains information about 
--    nfl teams
--	note : the expctd_fst_pick is the position
--		of the player expected to be draft 
----------

drop table nfl_team cascade constraints;

create table nfl_team
(team_id          number(2)    not null, 
 team_name        varchar2(40) not null, 
 conference       varchar2(15) not null,
 order_fst_pick   number(2)    not null check
 (order_fst_pick between 1 and 32), 
 expctd_fst_pick  char(2)      not null, 
 slry_cap_alwd    number(8)    not null,
 constraint nfl_team_pk primary key (team_id)
);


----------
-- College table contains the informations about colleges that have players in th$
-- note : a 0 for the season_rank means unranked
----------

drop table college cascade constraints;

create table college
(college_id         number(4)    not null,
 name               varchar2(20) not null,
 season_rank        number(2)    not null check
 (season_rank between 0 and 25),
 season_record      varchar2(5)  not null,
 num_of_play_draft  number(2)    not null,
 num_of_play_nfl    number(3)    not null,
 money_allowed      number(8)    not null,
 constraint college_pk primary key (college_id)
);

----------
-- player table contains information about the player
--	present in the draft
----------

drop table player cascade constraints;

create table player
(play_id	number(4)     not null,
 fst_name       varchar2(20)  not null, 
 lst_name       varchar2(20)  not null,
 age            number(2)     not null, 
 position       varchar(2)    not null,
 height         varchar2(5)   not null, 
 weight         number(3)     not null, 
 nfl_grade      number(3)     not null check
 (nfl_grade between 0 and 100), 
 is_eligible       varchar2(20) not null check
 (is_eligible in ('T', 'F')),
 college_id     number(4)    not null,
 constraint play_id_pk primary key (play_id),
 constraint player_fk_college_id foreign key (college_id) 
     references college
);


----------
-- test table contains information about the physical test of the player
-- note : the number in the bench_press column is the
--        number of repetitions at 225 lbs
-- note : the number for the squat is the
--         maximum weight in one rep lifting
-- note : is_sup is T for true if the player have more than expected stats for his position and F otherwise
-- note : the number reprensent the seconds for the
--        fty_dash (40 yards dash) and shuttle(20 yards shuttle)
-- note : the number represents the inches in the broad_jump
----------

drop table tests cascade constraints;

create table tests 
(test_id      	   number(3)    not null, 
 play_id      	   number(4)    not null,
 fty_dash          number(3,2)  not null, 
 shuttle           number(3,2)  not null,
 bench_press       number(2)    not null, 
 squat             number(3)    not null,
 broad_jump        number(3)    not null,
 vert_jump         number(4,2)  not null,
 is_sup            char(1)      not null check
 (is_sup in ('T', 'F')), 
 constraint tests_pk primary key (test_id), 
 constraint tests_fk_play_id foreign key (play_id) references player
);

----------
-- a row is added to test_needed when an player is has not past the physical check by a doctor yet
--     so when is_checked is F
--     the date_checked column should be null until the is_checked cell is T
-- note : is_good determine if we can change the 		  is_checked cell to 'T' or if the player will 		  have to
--        do another medical test
----------

drop table eligibility_check cascade constraints;

create table eligibility_check
(checking_id   number(5)    not null, 
 play_id       number(4)    not null, 
 is_good       char(1)      not null check
 (is_good in ('T', 'F')),
 date_asked    date         not null, 
 date_checked  date, 
 constraint eligibility_check_pk primary key (checking_id),
 constraint eligibility_check_fk_play_id foreign key (play_id) references player
);

----------
-- Top prospects table contains the rank and the strength of the top players
--     note : only 10 players on the top prospect
----------

drop table top_prospect cascade constraints;

create table top_prospect
(top_prospect_id  number(3)   not null, 
 play_id          number(4)   not null, 
 play_rank        number(2)   not null check
 (play_rank between 1 and 10), 
 big_strength    varchar2(20) not null, 
 constraint top_prospect_pk primary key (top_prospect_id), 
 constraint top_prospect_fk_play_id foreign key (play_id) references player
);

----------
-- nfl_draft_pick table contains informations about a draft pick
--	note: been trade inform us if the pick has been trade between two
--	      teams or not
----------

drop table nfl_draft_pick cascade constraints;

create table nfl_draft_pick
(nfl_draft_pick_id      number(2)   not null,
 play_id       		number(4)   not null,
 team_id		number(2)   not null,
 draft_round_num  	number(3)   not null,
 draft_pick_num  	number(3)   not null,
 been_trade   		char(1)   not null check
 (been_trade in ('Y', 'N')), 
 is_top_prospect 	char(1)   not null check
 (is_top_prospect in ('Y', 'N')),
 constraint nfl_draft_pick_pk primary key (nfl_draft_pick_id),
 constraint nfl_draft_pick_fk_play_id foreign key (play_id) references player,
 constraint nfl_draft_pick_fk_team_id foreign key (team_id) 
     references nfl_team
);

commit;
