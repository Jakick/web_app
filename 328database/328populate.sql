------------
-- 328populate.sql
--
-- populate the nfl draft database created by 328design.sql
--
-- by: Shyaka Laniesse
-- last modified: 2016-03-24
------------

delete from nfl_draft_pick;
delete from top_prospect;
delete from eligibility_check;
delete from tests;
delete from player;
delete from college;
delete from nfl_team;

-----
-- teams
-----

insert into nfl_team 
values 
(11, 'Dallas Cowboys', 'NFC East', 4, 'QB', 250000);

insert into nfl_team 
values 
(12, 'NY Giants', 'NFC East', 10, 'LB', 215000);

insert into nfl_team 
values 
(13, 'Philadelphia Eagles', 'NFC East', 13, 'DL', 200000);

insert into nfl_team 
values 
(14, 'Washington Redskins', 'NFC East', 21, 'RB', 187000);

insert into nfl_team 
values 
(21, 'Arizona Cardinals', 'NFC West', 30, 'OT', 180000);

insert into nfl_team 
values 
(22, 'LA Rams', 'NFC West', 15, 'DL', 250000);

insert into nfl_team 
values 
(23, 'SF 49ers', 'NFC West', 7, 'QB', 310000);

insert into nfl_team 
values 
(24, 'Seattle Seahawks', 'NFC West', 26, 'OT', 305000);

insert into nfl_team 
values 
(31, 'Chicago Bears', 'NFC North', 11, 'RB', 250000);

insert into nfl_team 
values 
(32, 'Detroit Lions', 'NFC North', 16, 'OT', 300000);

insert into nfl_team 
values 
(33, 'Green Bay Packers', 'NFC North', 27, 'DE', 170000);

insert into nfl_team 
values 
(34, 'Minnesota Vikings', 'NFC North', 23, 'K', 195000);

insert into nfl_team 
values 
(41, 'Atlanta Falcons', 'NFC South', 17, 'LB', 325000);

insert into nfl_team 
values 
(42, 'Carolina Panthers', 'NFC South', 30, 'PK', 270000);

insert into nfl_team 
values 
(43, 'New Orleans Saints', 'NFC South', 12, 'C', 324000);

insert into nfl_team 
values 
(44, 'Tampa Bay Buccanerrs', 'NFC South', 9, 'DT', 196000);

insert into nfl_team 
values 
(51, 'Buffalo Bills', 'AFC East', 19, 'DB', 216000);

insert into nfl_team 
values 
(52, 'Miami Dolphins', 'AFC East', 8, 'RB', 163000);

insert into nfl_team 
values 
(53, 'New England Patriots', 'AFC East', 32 , 'DE', 311000);

insert into nfl_team 
values 
(54, 'New York Jets', 'AFC East', 20, 'LB', 218000);

insert into nfl_team 
values 
(61, 'Denver Broncos', 'AFC WEST', 31, 'OG', 140000);

insert into nfl_team 
values 
(62, 'Kansas City Chiefs', 'AFC WEST', 28, 'OG', 182000);

insert into nfl_team 
values 
(63, 'Oakland Raiders', 'AFC WEST', 14, 'DB', 267000);

insert into nfl_team 
values 
(64, 'San Diego Chargers', 'AFC WEST', 3, 'QB', 341000);

insert into nfl_team 
values 
(71, 'Baltimore Ravens', 'AFC NORTH', 6, 'WR', 305000);

insert into nfl_team 
values 
(72, 'Cincinnati Bengals', 'AFC NORTH', 24, 'DB', 222000);

insert into nfl_team 
values 
(73, 'Cleveland Browns', 'AFC NORTH', 2, 'C', 349000);

insert into nfl_team 
values 
(74, 'Pittsburgh Steelers', 'AFC NORTH', 25, 'RB', 163000);

insert into nfl_team 
values 
(81, 'Houston Texans', 'AFC SOUTH', 22, 'QB', 184000);

insert into nfl_team 
values 
(82, 'Indianapolis Colts', 'AFC SOUTH', 18, 'RB', 196000);

insert into nfl_team 
values 
(83, 'Jacksonville Jaguars', 'AFC SOUTH', 5, 'DB', 360000);

insert into nfl_team 
values 
(84, 'Tennessee Titans', 'AFC SOUTH', 1, 'OT', 385000);


-- college



insert into college
values 
(1000, 'Florida', 14, '9-3', 7, 24, 5125000);

insert into college
values 
(1100, 'Ohio State', 2, '11-1', 6, 30, 325040);

insert into college
values 
(1200, 'Alabama', 3, '10-2', 8, 35, 550000);

insert into college
values 
(1300, 'Notre Dame', 8, '9-3', 6, 27, 325040);

insert into college
values 
(1400, 'Oregon', 18, '7-5', 4, 14, 555000);

insert into college
values 
(1500, 'Louisville', 10, '8-4', 4, 7, 270000);

insert into college
values 
(1600, 'Kansas State', 15, '7-5', 6, 16, 325040);

insert into college
values 
(1700, 'Florida State', 5, '9-3', 5, 21, 310000);

insert into college
values 
(1800, 'LSU', 4, '10-2', 10, 31, 400000);

insert into college
values 
(1900, 'Ole Miss', 7, '9-3', 5, 15, 330000);

insert into college
values 
(2000, 'Northwestern', 0, '5-7', 2, 8, 200000);

insert into college
values 
(2100, 'Missouri', 23, '7-5', 5, 12, 270000);

insert into college
values 
(2200, 'UCLA', 6, '9-3', 5, 18, 350000);

insert into college
values 
(2300, 'North Dakota', 13, '8-4', 3, 11, 280000);

insert into college
values 
(2400, 'Baylor', 9, '9-3', 5, 14, 405000);

insert into college
values 
(2500, 'Arkansas', 11, '8-4', 4, 21, 360000);

insert into college
values 
(2600, 'Stanford', 1, '11-1', 6, 25, 395000);

insert into college
values 
(2700, 'Georgia', 20, '7-5', 4, 19, 310000);

-----
-- initial player
-----

insert into player
values
(0110, 'Vernon', 'Hargreaves', 21, 'CB', '5-10', 204, 95, 'T', 1000);

insert into player
values
(0120, 'Eli', 'Apple', 21, 'CB', '6-1', 199, 92, 'T', 1100);

insert into player
values
(1110, 'Ryan', 'Kelly', 23, 'C', '6-4', 311, 96, 'T', 1200);

insert into player
values
(1120, 'Nick', 'Martin', 23, 'C', '6-4', 299, 94, 'T', 1300);

insert into player
values
(2110, 'Joey', 'Bosa', 21, 'DE', '6-5', 269, 94, 'T', 1100);

insert into player
values
(2120, 'DeForest', 'Buckner', 22, 'DE', '6-7', 291, 97, 'T', 1400);

insert into player
values
(3110, 'Sheldon', 'Rankins', 22, 'DT', '6-1', 299, 92, 'T', 1500);

insert into player
values
(3120, 'Ashawn', 'Robinson', 21, 'DT', '6-4', 307, 93, 'T', 1200);

insert into player
values
(4110, 'Glenn', 'Gronkowski', 22, 'RB', '6-2', 239, 85, 'T', 1600);

insert into player
values
(4120, 'Dan', 'Vitale', 22, 'RB', '6-1', 239, 80, 'T', 2000);

insert into player
values
(4130, 'Ezekiel', 'Elliot', 22, 'RB', '6-0', 225, 99, 'T', 1100);

insert into player
values
(4140, 'Derrick', 'Henry', 22, 'RB', '6-3', 247, 98, 'T', 1200);

insert into player
values
(5110, 'Jalen', 'Ramsey', 21, 'S', '6-1', 209, 93, 'T', 1700);

insert into player
values
(5120, 'Jalen', 'Mills', 22, 'S', '6-0', 191, 96, 'T', 1800);

insert into player
values
(5130, 'Vonn', 'Bell', 21, 'S', '5-11', 199, 86, 'T', 1100);

insert into player
values
(5140, 'Jeremy', 'Cash', 23, 'S', '6-0', 212, 85, 'T', 2500);

insert into player
values
(6110, 'Reggie', 'Ragland', 22, 'LB', '6-1', 247, 89, 'T', 1200);

insert into player
values
(6120, 'Kentrell', 'Brothers', 23, 'LB', '6-0', 232, 89, 'T', 2100);

insert into player
values
(6130, 'Myles', 'JackInjured', 21, 'LB', '6-1', 245, 98, 'T', 2200);

insert into player
values
(6140, 'Darron', 'Lee', 21, 'LB', '6-1', 232, 87, 'T', 1100);

insert into player
values
(7110, 'Cody', 'Whitehair', 23, 'OG', '6-4', 301, 89, 'T', 1600);

insert into player
values
(7120, 'Vadal', 'Alexander', 22, 'OG', '6-5', 326, 90, 'T', 1800);

insert into player
values
(8110, 'Laremy', 'Tunsil', 21, 'OT', '6-5', 310, 98, 'T', 1900);

insert into player
values
(8120, 'Ronnie', 'Stanley', 22, 'OT', '6-6', 312, 94, 'T', 1300);

insert into player
values
(9110, 'Jared', 'Goff', 21, 'QB', '6-4', 215, 95, 'T', 2200);

insert into player
values
(9120, 'Carson', 'Wentz', 22, 'QB', '6-5', 237, 88, 'F', 2300);

insert into player
values
(0111, 'Laquon', 'Treadwell', 21, 'WR', '6-2', 221, 99, 'T', 1900);

insert into player
values
(0112, 'Corey', 'Coleman', 22, 'WR', '5-11', 194, 92, 'T', 2400);

insert into player
values
(1111, 'Hunter', 'Henry', 21, 'TE', '6-5', 250, 90, 'T', 2500);

insert into player
values
(1112, 'Austin', 'Hooper', 21, 'TE', '6-4', 254, 89, 'T', 2600);

insert into player
values
(2111, 'Roberto', 'Aguayo', 22, 'K', '6-0', 207, 88, 'T', 1700);

insert into player
values
(2112, 'Kaimi', 'Fairbairn', 22, 'K', '5-11', 183, 87, 'T', 2200);

insert into player
values
(3111, 'Nathan', 'Theus', 23, 'LS', '6-3', 250, 88, 'T', 2700);

insert into player
values
(3112, 'Jimmy', 'Landes', 22, 'LS', '6-1', 240, 87, 'T', 2400);

-----
-- initial tests
-----

insert into tests
values 
(456, 0110, 4.46, 3.98, 15, 380, 130, 39.00, 'T');

insert into tests
values 
(457, 0120, 4.39, 4.02, 13, 375, 125, 38.90, 'T');

insert into tests
values 
(549, 1110, 5.45, 5.00, 28, 420, 98, 28.50, 'T');

insert into tests
values 
(550, 1120, 5.32, 5.10, 26, 410, 97, 27.20, 'T');

insert into tests
values 
(219, 2110, 4.85, 4.50, 25, 430, 100, 31.00, 'T');

insert into tests
values 
(220, 2120, 4.84, 4.47, 26, 425, 102, 30.50, 'T');

insert into tests
values 
(854, 3110, 5.02, 4.75, 30, 440, 95, 25.30, 'T');

insert into tests
values 
(855, 3120, 5.06, 4.69, 31, 435, 88, 24.70, 'T');

insert into tests
values 
(245, 4110, 4.56, 4.15, 24, 440, 119, 33.60, 'T');

insert into tests
values 
(246, 4120, 4.60, 4.17, 23, 445, 116, 30.80, 'T');

insert into tests
values 
(247, 4130, 4.45, 3.97, 25, 450, 125, 35.00, 'T');

insert into tests
values 
(248, 4140, 4.46, 4.00, 27, 460, 120, 32.00, 'T');

insert into tests
values 
(167, 5110, 4.40, 4.02, 24, 420, 130, 37.00, 'T');

insert into tests
values 
(168, 5120, 4.39, 3.99, 23, 410, 129, 37.60, 'T');

insert into tests
values 
(169, 5130, 4.42, 4.10, 27, 425, 127, 36.20, 'T');

insert into tests
values 
(170, 5140, 4.45, 4.13, 28, 440, 125, 37.10, 'T');

-----
-- initial eligibility_check
-----

insert into eligibility_check
values 
(11000, 0110, 'T', sysdate-20, sysdate-15);

insert into eligibility_check
values 
(11001, 0120, 'T', sysdate-10, sysdate-5);

insert into eligibility_check
values 
(11002, 1110, 'T', sysdate-17, sysdate-6);

insert into eligibility_check
values
(11003, 1120, 'T', sysdate-10, sysdate-1);

insert into eligibility_check
values
(11004, 2110, 'T', sysdate-15, sysdate-3);

insert into eligibility_check
values
(11005, 2120, 'T', sysdate-8, sysdate-2);

insert into eligibility_check
values
(11006, 3110, 'T', sysdate-25, sysdate-14);

insert into eligibility_check
values
(11007, 3120, 'T', sysdate-21, sysdate-12);

insert into eligibility_check
values
(11008, 4110, 'T', sysdate-10, sysdate-1);

insert into eligibility_check
values
(11009, 4120, 'T', sysdate-30, sysdate-20);

insert into eligibility_check
values
(11010, 4130, 'T', sysdate-22, sysdate-9);


-- top prospects

insert into top_prospect
values 
(354, 0110, 4, 'Speed');

insert into top_prospect
values 
(384, 3110, 10, 'Bull Rush');

insert into top_prospect
values 
(231, 2120, 2, 'Power');

insert into top_prospect
values
(222, 4130, 3, 'Playmaker');

insert into top_prospect
values
(152, 5120, 8, 'Lecture');

insert into top_prospect
values
(321, 6110, 9, 'Power');

insert into top_prospect
values
(126, 6120, 6, 'Speed');

insert into top_prospect
values
(367, 0111, 1, 'Physical');

insert into top_prospect
values
(366, 0112, 5, 'Hands');

insert into top_prospect
values
(101, 9110, 7, 'Arm Stregth');


-- nfl_draft_pick

insert into nfl_draft_pick
values 
(68, 0110, 11, 1, 4, 'N', 'Y');

insert into nfl_draft_pick
values 
(54, 0120, 12, 1, 10, 'N', 'N');

insert into nfl_draft_pick
values 
(69, 5110, 13, 1, 13, 'N', 'N');

insert into nfl_draft_pick
values
(12, 4130, 21, 2, 2, 'Y', 'Y');

insert into nfl_draft_pick
values
(10, 6120, 21, 1, 30, 'N', 'Y');

insert into nfl_draft_pick
values
(18, 0111, 22, 1, 15, 'N', 'Y');

insert into nfl_draft_pick
values
(29, 0112, 23, 1, 7, 'Y', 'N');

insert into nfl_draft_pick
values
(20, 9110, 24, 1, 26, 'N', 'Y');

insert into nfl_draft_pick
values
(90, 9120, 31, 2, 1, 'Y', 'N');

insert into nfl_draft_pick
values
(88, 8110, 31, 1, 11, 'N', 'N');

commit;
