<?php
    /*---
        function: create_player_dropdown
        purpose: expects an Oracle username and
            password (for HSU), and builds a drop-down
            of current players (and returns nothing)
    ---*/

    function create_eli_dropdown($username, $password)
    {
        $conn = hsu_conn_sess($username, $password);
        ?>

        <form method="post"
              action="<?= htmlentities($_SERVER['PHP_SELF'],
                                       ENT_QUOTES) ?>"
	      id="valueForm">
            <fieldset>
            <legend> Select player number </legend>
		<div>
	         <label> For statistical purposes, enter your age: </label>
		 <input type="text" name="age" id="age" required="required" />
		</div>
            <?php
            $player_query = "select fst_name, lst_name, play_id
                           from player";
            $player_stmt = oci_parse($conn, $player_query);
            oci_execute($player_stmt, OCI_DEFAULT);
            ?>

            <label for="play_choice"> Players: </label>
            <select name="play_choice" id="play_choice">

            <?php
            while (oci_fetch($player_stmt))
            {
                 $curr_fst_name = oci_result($player_stmt,
                                     "FST_NAME");
		 $curr_lst_name = oci_result($player_stmt,
                                     "LST_NAME");
		 $curr_play_id = oci_result($player_stmt,
                                     "PLAY_ID");
                 ?>

                 <option value="<?= $curr_play_id ?>">
                     <?= $curr_fst_name ?> <?= $curr_lst_name ?></option>
                 <?php
            }

            oci_free_statement($player_stmt);
            oci_close($conn);
            ?>
            </select>
            <div class="submit">
                <input type="submit" value="Get info" />
            </div>

            </fieldset>
        </form>
        <?php
    }
?>


