<?php
  /*--------
      function: create_player_info
      purpose: expects an entered Oracle username and
          password and a selected player, and
          queries for informations about the player
          and displays it "nicely"

      uses: hsu_conn_sess
  -------*/

function create_player_info($username, $password, $play_choice)
{
    // try to connect to Oracle student database

    $conn = hsu_conn_sess($username, $password);
            
    // try to carefully query for information on chosen
    //     department (note the use of a bind variable
    //     INSTEAD of concatenation!)

    $play_choice = strip_tags($play_choice);
    ?>

    <h2> Information about <?= $play_choice ?>: </h2>

    <?php
        $player_info_query = "select position, nfl_grade, age, name " .
			   "from player, college " .
			   "where player.fst_name = :play_choice " .
			   "and college.college_id = (select college_id " .
						     "from player " .
						     "where player.fst_name = :play_choice)";

    $player_info_stmt = oci_parse($conn, $player_info_query);

    oci_bind_by_name($player_info_stmt, ":play_choice", 
                     $play_choice);
        
    oci_execute($player_info_stmt, OCI_DEFAULT);
    ?>

    <table>
        <caption> Player infomations </caption>
        <tr> <th scope="col"> Position </th>
             <th scope="col"> Nfl_Grade </th>
             <th scope="col"> Age </th>
	     <th scope="col"> College </th> </tr>

    <?php
    	while (oci_fetch($player_info_stmt))
          {
    	$curr_pos = oci_result($player_info_stmt, "POSITION");
    	$curr_grade = oci_result($player_info_stmt, "NFL_GRADE");
    	$curr_age = oci_result($player_info_stmt, "AGE");
    	$curr_college = oci_result($player_info_stmt, "NAME");

    ?>

    <tr> <td> <?= $curr_pos ?> </td> 
         <td> <?= $curr_grade ?> </td>
         <td class="numeric"><?= $curr_age ?> </td>
	 <td> <?= $curr_college ?> </td>
            </tr>
            <?php
	     }
        ?>
    </table>
    <br />

    <form method="post"
              action="<?= htmlentities($_SERVER['PHP_SELF'],
                                       ENT_QUOTES) ?>">
            <div class="submit">
                <input type="submit" name="newChoice" value="New choice" />
	        <input type="submit" name="logback" value="Login" />
            </div>
    </form>

    <?php            
    oci_free_statement($player_info_stmt);
    oci_close($conn);
}
?>


