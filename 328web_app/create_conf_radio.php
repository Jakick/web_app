<?php
    /*---
        function: create_conf_radio
        purpose: expects an Oracle username and
            password (for HSU), and builds radio buttons
            of current conferences (and returns nothing)
    ---*/

    function create_conf_radio($username, $password)
    {
        $conn = hsu_conn_sess($username, $password);
        ?>

        <form method="post"
              action="<?= htmlentities($_SERVER['PHP_SELF'],
                                       ENT_QUOTES) ?>">
            <fieldset>
            <legend> Select a Conference </legend>

            <?php
            $conf_query = 'select conference
                           from nfl_team
			   group by conference';
            $conf_stmt = oci_parse($conn, $conf_query);
            oci_execute($conf_stmt, OCI_DEFAULT);
            ?>

            <p> Conferences : </p>

            <?php
            while (oci_fetch($conf_stmt))
            {
                 $curr_conf = oci_result($conf_stmt,
                                     "CONFERENCE");
                 ?>

		 <label>
                 <input type="radio" name="conf_choice"
			value="<?= $curr_conf ?>" />
                     <?= $curr_conf ?> 
	         </label> <br />

                 <?php
            }

            oci_free_statement($conf_stmt);
            oci_close($conn);
            ?>
            </select>

            <div class="submit">
                <input type="submit" value="Get info" />
            </div>

            </fieldset>
        </form>
        <?php
    }
?>
