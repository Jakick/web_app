<?php
    /*---
        function: create_login_form
        purpose: expects nothing, returns nothing,
                 and makes a form for a username
                 and password

        uses: name-pwd-fieldset.html
    ---*/

    function create_login_form()
    {
        ?>
        <form method="post"
              action="<?= htmlentities($_SERVER['PHP_SELF'],
                                       ENT_QUOTES) ?>">
        <?php
        require("name-pwd-fieldset.html");
        ?>
            <div class="submit">
                <input type="submit" value="Log in" />
            </div>
        </form>

        <?php
    }
?>
