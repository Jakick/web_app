"use strict";

/*-----
    add handlers to form AFTER page is 
        loaded
----*/
window.onload = 
    function()
    {
        // add a form-validation function to form

        var valueForm = 
            document.getElementById("valueForm");

	if (valueForm != null)
	{
            valueForm.onsubmit = checkAge; 
	}
    };

/*-----
    signature: checkAge: void -> Boolean
    purpose: expects nothing, returns true if
        the value enter in the age field is > 0;
        otherwise, it returns false AND complains
        in a NEWLY-ADDED errors paragraph

    by: Sharon Tuttle
    adapted by: Shyaka Laniesse
    last modified: 2016-05-01
-----*/

function checkAge()
{
    // get the DOM object for the document's body element

    var bodyObjectArray = 
        document.getElementsByTagName("body");
    var bodyObject = bodyObjectArray[0];

    // get the qty textfield

    var ageField = document.getElementById("age");

    var ageVal = ageField.value;
    var result = true;

    // create an errors paragraph unless one already
    //    exists

    var errorsPara = document.getElementById("errors");

    // if the above returned anything -- if thus error
    //   p element currently exists -- I hope it will
    //   be "truthy" here

    if (errorsPara)
    {
        // clear out its current contents

        errorsPara.innerHTML = "";

        // removing it from document until I see if
        //    I need it again

        bodyObject.removeChild(errorsPara);
    }

    // if I reach here, I should create this paragraph,
    //     just in case:

    else
    {
        errorsPara = document.createElement("p");

        errorsPara.id = "errors";
    }

    // complain and return false if any fields are empty

    if ( (! ageVal.trim() ))
    {
        errorsPara.innerHTML = "MUST fill in qty "
            +  "textfield before submit! (white space "
            +  "won't do)";
        result = false;
    }

    // if qty field is filled (with something non-white-
    //     space), then make it is numeric

    else if (isNaN(parseFloat(ageVal))
             ||
            (parseFloat(ageVal) != ageVal))
    {
        errorsPara.innerHTML = "Textfield MUST contain "
                               + "ONLY a number!";
        result = false;
    }

    // now check is the numeric value is superior than 0

    else if (parseFloat(ageVal) <= 0)
    {
        errorsPara.innerHTML = "Textfield MUST contain "
                               + "a number superior than 0";
        result = false;
    }

    // only add the errors paragraph if result is false

    if (result === false)
    {
        // get the DOM object for the form

        var formObject = document.getElementById("valueForm");

        // let's add the error paragraph as a child of
        //    body before the form

        bodyObject.insertBefore(errorsPara, formObject);
    }

    return result;
}  


