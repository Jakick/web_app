<?php
  /*--------
      function: create_eli_result
      purpose: expects an entered Oracle username and
          password and a selected player, and
	  call eligible_test

      uses: hsu_conn_sess
  -------*/

function create_eli_result($username, $password, $play_choice, $age)
{
    // try to connect to Oracle student database

    $conn = hsu_conn_sess($username, $password);
            
    // try to carefully query for information on chosen
    //     department (note the use of a bind variable
    //     INSTEAD of concatenation!)
    
    $age = strip_tags($age);
    $play_choice = strip_tags($play_choice);

    ?>
	 <p> Hi, you are <?= $age ?> years old and 
		the following information is for the player with the ID
		<b><?= $play_choice ?></b> </p>
    <?php
    
    $eligible_call = 'begin :isEli := bool_to_string(eligible_test(:play_choice)); end;';

    $eligible_stmt = oci_parse($conn, $eligible_call);

    oci_bind_by_name($eligible_stmt, ":play_choice",
                         $play_choice);
    oci_bind_by_name($eligible_stmt, ":isEli",
                         $isEli, 10);

    oci_execute($eligible_stmt, OCI_DEFAULT);
            
    if ($isEli == "TRUE")
    {
    ?>

        <p> The player is <b>eligible</b> </p>

    <?php
    }
    else
    {
    ?>
	<p> The player is not <b>eligible</b> </p>
    <?php
    }
    ?>

        <form method="post"
              action="<?= htmlentities($_SERVER['PHP_SELF'],
                                       ENT_QUOTES) ?>">
            <fieldset class="todo">

            <div class="submit">
                <input type="submit" name="logback" value="Login" />
                <input type="submit" name="newChoice" value="New Choice" />
            </div>

            </fieldset>
        </form>

    <?php            
    oci_free_statement($eligible_stmt);
    oci_close($conn);
}
?>

