<?php
  /*--------
      function: create_conf_info
      purpose: expects an entered Oracle username and
          password and a selected conference, and
          queries for informations about the teams in that conference
          and displays it "nicely"

      uses: hsu_conn_sess
  -------*/

function create_conf_info($username, $password, $conf_choice)
{
    // try to connect to Oracle student database

    $conn = hsu_conn_sess($username, $password);
            
    // try to carefully query for information on chosen
    //     department (note the use of a bind variable
    //     INSTEAD of concatenation!)

    $conf_choice = strip_tags($conf_choice);
    ?>

    <h2> Information about <?= $conf_choice ?>: </h2>

    <?php
        $conf_info_query = "select team_name, order_fst_pick, " .
			   "expctd_fst_pick, slry_cap_alwd " .
			   "from nfl_team " .
			   "where nfl_team.conference = :conf_choice";

    $conf_info_stmt = oci_parse($conn, $conf_info_query);

    oci_bind_by_name($conf_info_stmt, ":conf_choice", 
                     $conf_choice);
        
    oci_execute($conf_info_stmt, OCI_DEFAULT);
    ?>

    <table>
        <caption> Team infomations </caption>
        <tr> <th scope="col"> Name </th>
             <th scope="col"> Order pick 1st Round </th>
             <th scope="col"> Expected position pick </th>
	     <th scope="col"> Salary cap allowed </th> </tr>

    <?php
    	while (oci_fetch($conf_info_stmt))
          {
    	$curr_team = oci_result($conf_info_stmt, "TEAM_NAME");
    	$curr_num_pick = oci_result($conf_info_stmt, "ORDER_FST_PICK");
    	$curr_expctd_pos = oci_result($conf_info_stmt, "EXPCTD_FST_PICK");
    	$curr_slry_cap = oci_result($conf_info_stmt, "SLRY_CAP_ALWD");

    ?>

    <tr> <td> <?= $curr_team ?> </td> 
         <td> <?= $curr_num_pick  ?> </td>
         <td> <?= $curr_expctd_pos  ?> </td>
	 <td class="numeric"> $<?= $curr_slry_cap ?> </td>
            </tr>
            <?php
	     }
        ?>
    </table>
    <br />

    <form method="post"
              action="<?= htmlentities($_SERVER['PHP_SELF'],
                                       ENT_QUOTES) ?>">
            <div class="submit">
                <input type="submit" name="newChoice" value="New Choice" />
	        <input type="submit" name="logback" value="Log in" />
            </div>
    </form>

    <?php            
    oci_free_statement($conf_info_stmt);
    oci_close($conn);
}
?>

