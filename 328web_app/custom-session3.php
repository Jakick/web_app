<?php
    session_start();
?>

<!DOCTYPE html>
<html  xmlns="http://www.w3.org/1999/xhtml">

<!--
   using sessions and Oracle in a little PHP application
   URL to run custom-session3: 
	http://nrs-projects.humboldt.edu/~sl2686/hw11/custom-session3.php
   by: Sharon Tuttle
   modified by: Shyaka Laniesse 
   last modified: 2016-04-27
-->

<head>  
    <title> NFL Draft 2016 </title>
    <meta charset="utf-8" />

    <?php
        require_once("hsu_conn_sess.php");
        require_once("create_login_form.php");
	require_once("choice.php");
        require_once("create_player_dropdown.php");
	require_once("create_conf_radio.php");	
	require_once("create_eli_dropdown.php");
	require_once("create_conf_info.php");
        require_once("create_player_info.php");  
	require_once("create_eli_result.php");
    ?>

    <link rel="icon" href="arrow.png" />
    <link href="custom.css" 
          type="text/css" rel="stylesheet" />
    <link href="custom.css" type="text/css" 
          rel="stylesheet" />
    <script src="custom_js.js" type="text/javascript">
    </script>
</head> 

<body> 
    <h1> Get Draft info by Shyaka Laniesse for CS328 - NFL Draft Database </h1>

<?php

    // when this is 1st reached -- make a login form

    if ((! array_key_exists("username", $_POST)) and
        (! array_key_exists("next_screen", $_SESSION)))
    {
        create_login_form();
        $_SESSION["next_screen"] = "to_choose";
    }

    elseif ( (array_key_exists("username", $_POST)) and
	     ($_SESSION["next_screen"] == "to_choose"))
    {
        $username = strip_tags($_POST["username"]);
        $password = $_POST['password'];

        $_SESSION["username"] = $username;
        $_SESSION["password"] = $password;

        create_choice($username, $password);
    }

    elseif ((array_key_exists("confInfo", $_POST)) and
	    (array_key_exists("username", $_SESSION)) and
            ($_SESSION["next_screen"] == "to_choose"))
    {
	    $username = strip_tags($_SESSION['username']);
            $password = $_SESSION['password'];

       	    create_conf_radio($username, $password);

            $_SESSION["next_screen"] = "conf_chosen";
    }

    elseif ((array_key_exists("playInfo", $_POST)) and
            (array_key_exists("username", $_SESSION)) and
            ($_SESSION["next_screen"] == "to_choose"))
    {
            $username = strip_tags($_SESSION['username']);
            $password = $_SESSION['password'];

            create_player_dropdown($username, $password);

            $_SESSION["next_screen"] = "play_chosen";
    }

    elseif ((array_key_exists("eliTest", $_POST)) and
            (array_key_exists("username", $_SESSION)) and
            ($_SESSION["next_screen"] == "to_choose"))
    {
            $username = strip_tags($_SESSION['username']);
            $password = $_SESSION['password'];

            create_eli_dropdown($username, $password);

            $_SESSION["next_screen"] = "eli_chosen";
    }

    elseif (( $_SESSION["next_screen"] == "conf_chosen") and
            (array_key_exists("conf_choice", $_POST)) )
    {
        $conf_choice = strip_tags($_POST["conf_choice"]);

        $username = strip_tags($_SESSION['username']);
        $password = $_SESSION['password'];

        create_conf_info($username, $password, $conf_choice);
    }

    elseif (( $_SESSION["next_screen"] == "play_chosen") and
            (array_key_exists("play_choice", $_POST)) )
    {
        $play_choice = strip_tags($_POST["play_choice"]);

        $username = strip_tags($_SESSION['username']);
        $password = $_SESSION['password'];

        create_player_info($username, $password, $play_choice);
    }

    elseif ((  $_SESSION["next_screen"] == "eli_chosen") and
            (array_key_exists("play_choice", $_POST)) )
    {
        $play_choice = strip_tags($_POST["play_choice"]);
	$age = strip_tags($_POST["age"]);

        $username = strip_tags($_SESSION['username']);
        $password = $_SESSION['password'];

        create_eli_result($username, $password, $play_choice, $age);
    }
    elseif (array_key_exists("newChoice", $_POST))
    {
        $username = strip_tags($_SESSION['username']);
        $password = $_SESSION['password'];

	$_SESSION["next_screen"] = "to_choose";
        create_choice($username, $password);
    }

    elseif (array_key_exists("logback", $_POST))
    {
        create_login_form();

	session_destroy();
	session_regenerate_id(TRUE);
	session_start();

	$_SESSION["next_screen"] = "to_choose";
    }

    else
    {
        create_login_form();

        // since should not get here "normally",
        //    we'll destroy and restart session in
        //    this case

        session_destroy();
        session_regenerate_id(TRUE);
        session_start();

	$_SESSION["next_screen"] = "to_choose";
    }

    require_once("328footer-better.html");
    ?>

</body>
</html>
