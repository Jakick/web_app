<?php
    session_start();
?>

<!DOCTYPE html>
<html  xmlns="http://www.w3.org/1999/xhtml">

<!--
    using session attributes to control a 
    a pizza order

    by: Shyaka Laniesse
    last modified: 2016-04-21
-->

<head>  
    <title> Ordering Pizza </title>
    <meta charset="utf-8" />

    <?php
        require_once("pizza_choice_form.php");
        require_once("pizza_paiement.php");
	require_once("pizza_resume.php");
    ?>

    <link href="pizza.css" 
          type="text/css" rel="stylesheet" />
</head> 

<body> 

    <h1> Pizza Order </h1>

    <?php

    if (! array_key_exists('next_page', $_SESSION))
    {
        pizza_choice();
        $_SESSION['next_page'] = "paiement";
    }

    elseif (($_SESSION['next_page'] == "paiement") and
           (array_key_exists("lastname", $_POST)) and
           (array_key_exists("size", $_POST)) and
           (array_key_exists("crust", $_POST)) and
           (array_key_exists("sauce", $_POST)) and
           (array_key_exists("toppings", $_POST)) and
           (array_key_exists("qty", $_POST)) )
    {
        pizza_paiement();
	$_SESSION['next_page'] = "resume";
    }

    elseif (($_SESSION['next_page'] == "resume") and
	   (array_key_exists("cardnumber", $_POST)) )
    {
        pizza_resume();
        session_destroy();
    }


    else
    {

        session_destroy();
        session_regenerate_id(TRUE);
        session_start();

	pizza_choice();
        $_SESSION['next_page'] = "paiement";
    }

    require_once("328footer-better.html");
?>

</body>
</html>
