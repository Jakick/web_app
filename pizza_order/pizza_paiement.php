<?php
    /*-----
        function: request_quest
        purpose: expects nothing, returns nothing,
            but has the side-effects of asking
	    paiement information of a client
    -----*/
    
    function pizza_paiement()
    { 

        $name = strip_tags($_POST['lastname']);
        $_SESSION['name'] = $name;

	$size = strip_tags($_POST['size']);
        $_SESSION['size'] = $size;

	$crust = strip_tags($_POST['crust']);
        $_SESSION['crust'] = $crust;

	$sauce = strip_tags($_POST['sauce']);
        $_SESSION['sauce'] = $sauce;

	$count = sizeof($_POST['toppings']);
	$_SESSION['count'] = $count;

	$qty = strip_tags($_POST['qty']);
        $_SESSION['qty'] = $qty;	

	$tot_price = 9.99;
	
	if ($size == "8")
	{
	    $tot_price += 4;
	}
	elseif ($size == "12")
        {
            $tot_price += 8.10;
        }
	elseif ($size == "18")
        {
            $tot_price += 12.70;
        }
	else
	{
	    $tot_price += 16.50;
        }

	if ($count <= 3)
	{
	    $tot_price +=3;
	}
	elseif ($count >= 4 AND $count <= 8)
	{
	     $tot_price +=6.20;
        }
	else
	{
	    $tot_price += 10.50;
	}

	$_SESSION['price'] = $tot_price;

	$tot_price *= $qty;
	$_SESSION['tot_price'] = $tot_price;
	
        ?>

        <h2> 
            Hi <?= $name ?>, your total price is:$<?= $tot_price ?>
        </h2>

        <form method="post"
              action="<?= htmlentities($_SERVER['PHP_SELF'],
                                       ENT_QUOTES) ?>">

	    <fieldset>
            <legend> Paiement information </legend>
            <label> Credit card number: (XXXX XXXX XXXX XXXX)
	    <input type="text" name="cardnumber"
                   required="required"
                   maxlength="19" /> </label> <br />
        </fieldset>


        <fieldset>
            <legend> Expiration </legend>
            <label> Month 
                <select name="month">
                    <option value="jan" selected="selected">
                        January </option>
                    <option value="feb"> February </option>
                    <option value="mar"> March </option>
                    <option value="apr"> April </option>
		    <option value="may"> May </option>
                    <option value="jun"> June </option>
                    <option value="jul"> July </option>
		    <option value="aug"> August </option>
                    <option value="sep"> September </option>
                    <option value="nov"> November </option>
		    <option value="dec"> December </option>
                </select>
            </label>
	
	    <label> Year
                <select name="years">
                    <option value="16" selected="selected">
                        2016 </option>
                    <option value="17"> 2017 </option>
                    <option value="18"> 2018 </option>
                    <option value="19"> 2019 </option>
                    <option value="20"> 2020 </option>
                    <option value="21"> 2021 </option>
                    <option value="22"> 2022 </option>
                    <option value="23"> 2023 </option>
                    <option value="24"> 2024 </option>
                    <option value="25"> 2025 </option>
                    <option value="26"> 2026 </option>
                </select>
            </label>
        </fieldset>

            <input type="submit" value="Proceed paiement" />
        </form>

	<form action="<?= htmlentities($_SERVER['PHP_SELF'],
                                       ENT_QUOTES) ?>">
    		<input type="submit" value="Cancel">
	</form>
        <?php
    }
?>
