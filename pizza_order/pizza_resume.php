<?php
   /*-----
        function: pizza_resume
        purpose: expects nothing, returns nothing,
	    but has the side-effects of print out
	    information about the order
   -----*/

    function pizza_resume()
    {
	$name = $_SESSION['name'];
	$size = $_SESSION['size'];
	$crust = $_SESSION['crust'];
	$sauce = $_SESSION['sauce'];
	$count = $_SESSION['count'];
	$tot_price = $_SESSION['tot_price'];
	$price = $_SESSION['price'];
	
	?>

	<h2> Thank you <?= $name ?>, your order has been placed: </h2>

	<?php
	
	    if ($count <= 3)
	    {
	?>
		 <p> Your number of toppings is <?= $count ?> and 
		 correspond to a <b>LIGHT</b> pizza </p>
	<?php
	    }
	    elseif ($count >= 4 AND $count <= 8)
	    {
	?>
		<p> Your number of toppings is <?= $count ?> and 
	  	correspond to a <b>DECENT</b> pizza </p>
	 <?php
	    }
	    else
	    {
	?>
		<p>  Your number of toppings is <?= $count ?> .. 
		That's a <b>HUGE</b> Pizza, you re a beast </p>
	<?php
	    }
	?>
	
	<p> Quick resume of your pizza: </p>
	<ul>
            <li> Size: <?= $size ?> inches </li>
            <li> Crust: <?= $crust ?> </li>
	    <li> Sauce: <?= $sauce ?> </li>
	    <li> Price/pizza: $<?= $price ?> </li>
	    <li> Total: $<?= $tot_price ?> </li>
    	</ul>

	</form>

        <form action="<?= htmlentities($_SERVER['PHP_SELF'],
                                       ENT_QUOTES) ?>">
                <input type="submit" value="New Order">
        </form>

	<?php
    }
?>
