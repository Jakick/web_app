<?php

    function pizza_choice()
    {
	?>
        <form method="post"
              action="<?= htmlentities($_SERVER['PHP_SELF'],
                                       ENT_QUOTES) ?>">

	 <fieldset>
            <legend> Client </legend>
            <label> Last Name:
                <input type="text" name="lastname"
                       required="required" />
            </label> <br />
	    <label> Phone number: (XXX)(XXX)(XXXX)
                <input type="number" name="phone1"
                       required="required" max="999" /> </label>
		<input type="number" name="phone2"
                       required="required" max="999" />
		<input type="number" name="phone3"
                       required="required" max="9999" />
            <br />
        </fieldset>

        <fieldset>
            <legend> Address </legend>
            <label> Street Address:
                <input type="text" name="saddress" 
                       required="required" /> 
            </label> <br />

            <label> City:
                <input type="text" name="city" 
		       required="required" />
            </label> <br />

            <label> Zip: 
            <input type="number" name="zip" 
                   required="required"
                   max="99999" /> 
	    </label> <br />
        
        </fieldset>

	<fieldset>
            <legend> Choose your size </legend>

            <label>
                <input type="radio" name="size"
                       value="8"
                       checked="checked" />
                8
            </label> <br />

            <label>
                <input type="radio" name="size"
                       value="12" />
                12
            </label> <br />

            <label>
                <input type="radio" name="size"
                       value="18" />
                 18
            </label> <br />
	
	    <label>
                <input type="radio" name="size"
                       value="21" />
                 21
            </label> <br />

        </fieldset>

        <fieldset>
            <legend> Choose your crust </legend>

            <label> 
                <input type="radio" name="crust"
                       value="Thin"
                       checked="checked" />
                Thin
            </label> <br />

            <label> 
                <input type="radio" name="crust"
                       value="Crispy" />
                Crispy
            </label> <br />

            <label> 
                <input type="radio" name="crust"
                       value="Chewy" />
                 Chewy
            </label> <br />
        </fieldset>

	<fieldset>
            <legend> Choose your sauce </legend>

            <label>
                <input type="radio" name="sauce"
                       value="Red"
                       checked="checked" />
                Red (tomato)
            </label> <br />

            <label>
                <input type="radio" name="sauce"
                       value="White" />
                White (four cheese)
            </label> <br />

            <label>
                <input type="radio" name="sauce"
                       value="Curry" />
                 Curry
            </label> <br />
        </fieldset>

        <fieldset>
           <legend> Choose your toppings </legend>

           <label>
               <input type="checkbox" name="toppings[]" 
                      checked="checked" />
               Merguez
           </label>

           <label>
               <input type="checkbox" name="toppings[]" />
               Bacon
           </label>

           <label>
               <input type="checkbox" name="toppings[]" />
               Ground beef
           </label> 

           <label>
               <input type="checkbox" name="toppings[]" />
               Chicken
           </label>

	   <label>
               <input type="checkbox" name="toppings[]" />
               Tuna
           </label> <br />

	   <label>
               <input type="checkbox" name="toppings[]" />
               Mushrooms
           </label>

	   <label>
               <input type="checkbox" name="toppings[]" />
               Onions
           </label>

	   <label>
               <input type="checkbox" name="toppings[]" />
               Cheese
           </label>

	   <label>
               <input type="checkbox" name="toppings[]" />
               Green pepers
           </label>

	   <label>
               <input type="checkbox" name="toppings[]" />
               Fresh tomatoes
           </label> <br />

        </fieldset>

	<fieldset>
	    <legend> Pizza Price </legend>	    
       	    <input type="text" name="pprice"
               	   required="required"
		   value="$0.00"
              	   readonly/> <br />
        </fieldset>

	<fieldset>
            <legend> Quantity </legend>
            <input type="number" name="qty"
                   required="required"
		   value="1"
                   min="1" max="20" /> <br />
        </fieldset>

	<fieldset>
            <legend> Total Price </legend>
            <input type="text" name="pprice"
                   required="required"
                   value="$0.00"
                   readonly /> <br />
        </fieldset>

	<input type="submit" name="order"
               value="Go to paiment" />

        </form>
       <?php
    }
?>
